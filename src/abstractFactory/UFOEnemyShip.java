package abstractFactory;

public class UFOEnemyShip extends EnemyShip {
	EnemyShipFactory shipFactory;

	public UFOEnemyShip(EnemyShipFactory newShipFactory) {
		shipFactory = newShipFactory;
	}

	@Override
	void makeShip() {
		System.out.println("Making enemy ship " + getName());
		weapon = shipFactory.addESGun();
		engine = shipFactory.addESEngine();
	}

}
