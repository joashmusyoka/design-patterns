package abstractFactory;

public class EnemyShipTesting {

	public static void main(String[] args) {
		EnemyShipBuilding makeUfos  = new UFOEnemyShipBuilding();
		EnemyShip theGrunt = makeUfos.orderTheShip("UFO");
		
		System.out.println(theGrunt+ "\n");
		EnemyShip theBoss = makeUfos.orderTheShip("UFO BOSS");
		System.out.println(theBoss+ "\n");
	}

}
