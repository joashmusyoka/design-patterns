package adapter;

public class TestEnemyAttacker {

	public static void main(String[] args) {
		/*
		 * This is a regular Enemy Attacker
		 */
		EnemyTank rx7Tank = new EnemyTank();

		/*
		 * create an enemy robot.
		 */
		EnemyRobot fredTheRobot = new EnemyRobot();
		/*
		 * create an enemy robot adapter to make the robot work like a normal
		 * enemy attacker
		 */
		EnemyRobotAdapter adapter = new EnemyRobotAdapter(fredTheRobot);
		
		System.out.println("The Robot:");
		fredTheRobot.reactToHuman("Paul");
		fredTheRobot.walkForward();
		fredTheRobot.smashWithHands();
		System.out.println();
		
		System.out.println("The enemy tank:");
		rx7Tank.assignDriver("Frank");
		rx7Tank.driveForward();
		rx7Tank.fireWeapon();
		System.out.println();
		
		System.out.println("The robot with adapter:");
		adapter.assignDriver("Mark");
		adapter.driveForward();
		adapter.fireWeapon();
		System.out.println();
	}

}
