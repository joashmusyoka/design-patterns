package adapter;

import java.util.Random;

/*
 * This is the adaptee. It needs to be adapted to work like an EnemyAttacker
 * */
public class EnemyRobot {
	Random generator = new Random();

	public void smashWithHands() {
		int attackDamage = generator.nextInt(10) + 1;

		System.out.println("Enemy tank causes " + attackDamage
				+ " damage with its hands");
	}

	public void walkForward() {
		int movement = generator.nextInt(5) + 1;

		System.out.println("Enemy tank walks " + movement + " steps");
	}
	
	public void reactToHuman(String driverName){
		System.out.println("Enemy robot tramps on "+driverName);
		
	}
}
