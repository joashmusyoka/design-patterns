package adapter;

/*
 * This is the adapter. 
 * It uses composition to store an instance of the adaptee.
 * When a method of the implemented interface is called, its calls the corresponding method of the adaptee
 * */
public class EnemyRobotAdapter implements EnemyAttacker {

	EnemyRobot theRobot;

	public EnemyRobotAdapter(EnemyRobot newRobot) {
		theRobot = newRobot;
	}

	@Override
	public void fireWeapon() {
		theRobot.smashWithHands();
	}

	@Override
	public void driveForward() {
		theRobot.walkForward();
	}

	@Override
	public void assignDriver(String driverName) {
		theRobot.reactToHuman(driverName);
	}

}
