package adapter;

/*
 * This is what the client expects to work with. The client expects all the methods to be implmented
 * */
public interface EnemyAttacker {
	public void fireWeapon();

	public void driveForward();

	public void assignDriver(String driverName);
}
