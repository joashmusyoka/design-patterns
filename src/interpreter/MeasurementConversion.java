package interpreter;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MeasurementConversion {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		JFrame frame = new JFrame();

		String questionAsked = JOptionPane.showInputDialog(frame,
				"What is your question?");

		ConversionContext question = new ConversionContext(questionAsked);

		String fromConversion = question.getFromConversion();
		String toConversion = question.getToConversion();
		double quantity = question.getQuantity();

		// get the class based on the fromConversion string; reflection
		try {
			Class tempClass = Class.forName("interpreter."+fromConversion);

			Constructor con = tempClass.getConstructor();

			Object cnvertFrom = (Expression) con.newInstance();

			Class[] methodParams = new Class[] { Double.TYPE };

			// get the method dymanically
			Method conversionMethod = tempClass.getMethod(toConversion,
					methodParams);

			Object[] params = new Object[] { new Double(quantity) };
			String toQuantity = (String) conversionMethod.invoke(cnvertFrom,
					params);

			String answerToQues = question.getConversionResponse() + toQuantity
					+ " " + toConversion;

			JOptionPane.showMessageDialog(null, answerToQues);
			frame.dispose();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
