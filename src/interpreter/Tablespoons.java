package interpreter;

public class Tablespoons extends Expression {

	@Override
	public String gallons(double quantity) {
		return Double.toString(quantity);
	}

	@Override
	public String quarts(double quantity) {
		return Double.toString(quantity/10);
	}

	@Override
	public String pints(double quantity) {
		return Double.toString(quantity/20);
	}

	@Override
	public String cups(double quantity) {
		return Double.toString(quantity/30);
	}

	@Override
	public String tablespoons(double quantity) {
		return Double.toString(quantity/40);
	}

}
