package interpreter;

public class Pints extends Expression {

	@Override
	public String gallons(double quantity) {
		return Double.toString(quantity);
	}

	@Override
	public String quarts(double quantity) {
		return Double.toString(quantity/32);
	}

	@Override
	public String pints(double quantity) {
		return Double.toString(quantity/64);
	}

	@Override
	public String cups(double quantity) {
		return Double.toString(quantity/128);
	}

	@Override
	public String tablespoons(double quantity) {
		return Double.toString(quantity/256);
	}

}
