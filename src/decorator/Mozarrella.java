package decorator;

public class Mozarrella extends ToppingDecorator {

	public Mozarrella(Pizza newPizza) {
		super(newPizza);
		
		System.out.println("Adding Dough");
		
		System.out.println("Adding Moz"); 
		
	}
	
	public String getDescription(){
		
		return tempPizza.getDescription() + ", Mozarella";
		
	}
	
	public double getCost(){
		
		return tempPizza.getCost() + .50 ;
		
	}

}
