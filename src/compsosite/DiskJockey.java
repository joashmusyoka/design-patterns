package compsosite;

public class DiskJockey {
	SongComposite songList;

	public DiskJockey(SongComposite newSongList) {
		songList = newSongList;
	}

	public void getSongList() {
		songList.displaySongInfo();
	}
}
