package compsosite;

public abstract class SongComposite {
	/*
	 * A SongComposite can be a SongGroup or a Song
	 * */
	public void add(SongComposite newSongComponent) {
		throw new UnsupportedOperationException();
	}
	
	public void remove(SongComposite newSongComponent) {
		throw new UnsupportedOperationException();
	}
	
	public SongComposite get(int componentIndex) {
		throw new UnsupportedOperationException();
	}
	
	public String getSongName() {
		throw new UnsupportedOperationException();
	}
	
	public String getBandName() {
		throw new UnsupportedOperationException();
	}
	
	public int getReleaseYear() {
		throw new UnsupportedOperationException();
	}
	
	public void displaySongInfo() {
		throw new UnsupportedOperationException();
	}
}
