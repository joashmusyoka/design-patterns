package compsosite;

public class SongListGenerator {
	public static void main(String[] args) {
		SongComposite industrialMusic = new SongGroup(
				"Industrial", 
				"is a style of experimental music that draws on transgressive and proactive themes."
			);
		SongComposite heaveyMetalMusic = new SongGroup(
				"Heavy Metal", 
				"is a genre of rock music that developed in late 1960's largely in the UK and in the USA."
		);
		SongComposite dubstepMusic = new SongGroup(
				"Dubstep",
				"is a genre of electronic music that originated in South London, England."
		);
		
		SongComposite everySong = new SongGroup(
				"Song List", 
				"Every Song Available"
		);
		
		everySong.add(industrialMusic);
		
		industrialMusic.add(new Song("Head Like a Hole","NIN", 1990));
		industrialMusic.add(new Song("Headhunter","Front 242", 1988));
		
		/*Add a sub group to industrial music*/
		industrialMusic.add(dubstepMusic);
		dubstepMusic.add(new Song("Centipede", "Knife Party", 2012));
		dubstepMusic.add(new Song("Tetris", "Doctor P", 2011));
		
		everySong.add(heaveyMetalMusic);
		heaveyMetalMusic.add(new Song("War Pigs", "Black Sabath", 1970));
		heaveyMetalMusic.add(new Song("Ace of Spades", "Motorhead", 1980));
		
		DiskJockey crazyLarry = new DiskJockey(everySong);
		crazyLarry.getSongList();
	}
}
