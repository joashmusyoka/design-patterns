package compsosite;

import java.util.ArrayList;
import java.util.Iterator;

public class SongGroup extends SongComposite {
	ArrayList<SongComposite> songComponents = new ArrayList<SongComposite>();
	String groupName;
	String groupDescription;

	public SongGroup(String newGroupName, String newGroupDescription) {
		groupName = newGroupName;
		groupDescription = newGroupDescription;
	}

	public String getGroupName() {
		return groupName;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	@Override
	public void add(SongComposite songComponent) {
		songComponents.add(songComponent);
	}

	@Override
	public void remove(SongComposite songComponent) {
		songComponents.remove(songComponent);
	}

	@Override
	public SongComposite get(int componentIndex) {
		return songComponents.get(componentIndex);
	}
	
	@Override
	public void displaySongInfo(){
		System.out.println(
				getGroupName()+" "+getGroupDescription()+"\n"
		);
		
		Iterator<SongComposite> songIterator = songComponents.iterator();
		while(songIterator.hasNext()){
			 SongComposite songInfo = songIterator.next();
			 songInfo.displaySongInfo();
		}
		
	}
}
