package proxy;

public class TestATMMachineProxy {
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		GetATMData realATMMachine = new ATMMachine();
		GetATMData atmProxy = new ATMProxy();

		System.out.println("\nCurrent ATM State: " + atmProxy.getATMData());
		System.out.println("\nCAsh in Machine: Ksh. " + atmProxy.getCashInMachine());

		/*
		 * realATMMachine.setCashInMachine() Will not work as well because
		 * ATMMAchine is part of the interface GetATMData So by implementing the
		 * interface, we limit the available methods to those that we deem
		 * secure
		 */
	}
}
