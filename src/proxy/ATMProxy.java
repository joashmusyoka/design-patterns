package proxy;
/*
 * The proxy will be able to both create and destroy ATMMachine objects
 * It will then provide access to those methods that we consider to be safe
 * */
public class ATMProxy implements GetATMData{

	@Override
	public ATMState getATMData() {
		ATMMachine realATMMachine = new ATMMachine();
		
		return realATMMachine.getATMData();
	}

	@Override
	public int getCashInMachine() {
		ATMMachine realATMMachine = new ATMMachine();
		return realATMMachine.getCashInMachine();
	}

}
