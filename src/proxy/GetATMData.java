package proxy;

/*
 * This interface defines the safe methods in regards to the ATM
 * This would for example be in the office of a bank manager who needs to know about ATM statues
 * */
public interface GetATMData {
	public ATMState getATMData();
	public int getCashInMachine();
}
