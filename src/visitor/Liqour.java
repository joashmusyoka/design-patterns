package visitor;

public class Liqour implements Visitable {
	private double price;

	Liqour(double itemPrice) {
		price = itemPrice;
	}

	public double getPrice() {
		return price;
	}

	@Override
	public double accept(Visitor visitor) {
		
		return visitor.visit(this);
	}

}
