package visitor;

public class Tobacco implements Visitable {
	private double price;

	Tobacco(double itemPrice) {
		price = itemPrice;
	}

	public double getPrice() {
		return price;
	}

	@Override
	public double accept(Visitor visitor) {
		
		return visitor.visit(this);
	}

}