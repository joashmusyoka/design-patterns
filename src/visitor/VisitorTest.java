package visitor;

public class VisitorTest {

	public static void main(String[] args) {
		TaxVisitor taxCalc = new TaxVisitor();
		TaxHolidayVisitor taxHolidayCalc = new TaxHolidayVisitor();
		
		Necessity milk = new Necessity(3.47);
		Tobacco safari = new Tobacco(2.5);
		Liqour tusker = new Liqour(5.8);
		
		System.out.println("NORMAL PRICES:");
		System.out.println(milk.accept(taxCalc) + "\n");
		System.out.println(safari.accept(taxCalc) + "\n");
		System.out.println(tusker.accept(taxCalc) + "\n");
		
		System.out.println("TAX HOLIDAY PRICES:");
		System.out.println(milk.accept(taxHolidayCalc) + "\n");
		System.out.println(safari.accept(taxHolidayCalc) + "\n");
		System.out.println(tusker.accept(taxHolidayCalc) + "\n");
	}

}
