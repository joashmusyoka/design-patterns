package visitor;

public interface Visitor {
	public double visit(Liqour liquorItem);
	public double visit(Tobacco tubaccoItem);
	public double visit(Necessity necessityItem);
}
