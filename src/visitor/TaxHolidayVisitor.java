package visitor;

import java.text.DecimalFormat;

public class TaxHolidayVisitor implements Visitor{
	DecimalFormat df = new DecimalFormat("#.##");

	public TaxHolidayVisitor() {
	}

	@Override
	public double visit(Liqour liquorItem) {
		System.out.println("Liquor Item: Price with Tax Holiday");

		return Double.parseDouble(df.format(liquorItem.getPrice() * .10
				+ liquorItem.getPrice()));
	}

	@Override
	public double visit(Tobacco tubaccoItem) {
		System.out.println("Tobacco Item: Price with Tax Holiday");

		return Double.parseDouble(df.format(tubaccoItem.getPrice() * .20
				+ tubaccoItem.getPrice()));
	}

	@Override
	public double visit(Necessity necessityItem) {
		System.out.println("Necessity Item: Price with Tax Holiday");

		return Double.parseDouble(df.format(necessityItem.getPrice() * 0
				+ necessityItem.getPrice()));

	}

}
