package template.method;

public abstract class Hoagie {
	/*
	 * Final so sub-classes may not change it
	 */
	public final void makeSandWitch() {
		/*This is not optional*/
		cutBun();

		if (customerWantsMeat()) {
			addMeat();
		}

		if (customerWantsCheese()) {
			addCheese();
		}
		if (customerWantsVegetables()) {
			addVegetables();
		}
		if (customerWantsCondiments()) {
			addCondiments();
		}
		/*This is also not optional*/
		wrapTheHoagie();
	}
	public void cutBun(){
		System.out.println("The Hoagie is cut.");
	}

	public abstract void addMeat();

	public abstract void addCheese();

	public abstract void addVegetables();

	public abstract void addCondiments();

	/*
	 * A hook to specify if the customer wants meat
	 */
	boolean customerWantsMeat() {
		return true;
	}

	/*
	 * A hook to specify if the customer wants cheese
	 */
	boolean customerWantsCheese() {
		return true;
	}

	/*
	 * A hook to specify if the customer wants vegetables
	 */
	boolean customerWantsVegetables() {
		return true;
	}

	/*
	 * A hook to specify if the customer wants condiments
	 */
	boolean customerWantsCondiments() {
		return true;
	}

	public void wrapTheHoagie(){
		System.out.println("The Hoagie is wrapped.");
	}
}
