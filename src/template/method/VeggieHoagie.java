package template.method;

public class VeggieHoagie extends Hoagie {

	String[] veggiesUsed = {"Lettuce", "Tomatoes" , "Onions", "Sweet Pepper"};
	String[] condimentsUsed = {"Oil", "Vinegar"};
	
	boolean customerWantsMeat() {
		return false;
	}
	boolean customerWantsCheese() {
		return false;
	}

	@Override
	public void addMeat() {}

	@Override
	public void addCheese() {}

	@Override
	public void addVegetables() {
		System.out.println("Adding the vegitables: ");
		for(String veg: veggiesUsed){
			System.out.println(veg + " ");
		}

	}

	@Override
	public void addCondiments() {
		System.out.println("Adding the condiments: ");
		for(String condiment: condimentsUsed){
			System.out.println(condiment + " ");
		}

	}

}
