package template.method;

public class SandwitchSculptor {

	public static void main(String[] args) {
	
		Hoagie cust12Hoagie = new ItalianHoagie();
		cust12Hoagie.makeSandWitch();
		
		System.out.println("\n");
		
		Hoagie cust13Hoagie = new VeggieHoagie();
		cust13Hoagie.makeSandWitch();

	}

}
