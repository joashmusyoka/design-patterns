package template.method;

public class ItalianHoagie extends Hoagie {
	String[] meatUsed = {"Salami", "Pepperoni" , "Capicola Ham"};
	String[] cheeseUsed = {"Provolone"};
	String[] veggiesUsed = {"Lettuce", "Tomatoes" , "Onions", "Sweet Pepper"};
	String[] condimentsUsed = {"Oil", "Vinegar"};

	@Override
	public void addMeat() {
		System.out.println("Adding the meat: ");
		for(String meat: meatUsed){
			System.out.println(meat + " ");
		}

	}

	@Override
	public void addCheese() {
		System.out.println("Adding the cheese: ");
		for(String cheese: cheeseUsed){
			System.out.println(cheese + " ");
		}

	}

	@Override
	public void addVegetables() {
		System.out.println("Adding the vegitables: ");
		for(String veg: veggiesUsed){
			System.out.println(veg + " ");
		}

	}

	@Override
	public void addCondiments() {
		System.out.println("Adding the condiments: ");
		for(String condiment: condimentsUsed){
			System.out.println(condiment + " ");
		}

	}

}
