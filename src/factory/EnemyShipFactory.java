package factory;

public class EnemyShipFactory {
	public EnemyShip makeEnemyShip(String newShipType) {
		if (newShipType.equalsIgnoreCase("U"))
			return new UFOEnemyShip();

		if (newShipType.equalsIgnoreCase("R"))
			return new RocketEnemyShip();

		if (newShipType.equalsIgnoreCase("B"))
			return new BigUFOEnemyShip();
		return null;
	}
}
