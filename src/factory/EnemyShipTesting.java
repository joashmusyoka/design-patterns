package factory;

import java.util.Scanner;

@SuppressWarnings("resource")
public class EnemyShipTesting {
	
	public static void main(String[] args){
		/*
		 * EnemyShip ufoShip = new UFOEnemyShip();
		 * This is the old style way of creating objects.
		 * 
		 * */	
		
		EnemyShipFactory shipFactory = new EnemyShipFactory();
		
		EnemyShip theEnemy = null;
	
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("What type of ship? (U / R / B)");
		
		if(userInput.hasNextLine()){
			
			String typeOfShip = userInput.nextLine();
			
			theEnemy = shipFactory.makeEnemyShip(typeOfShip);
			
		}
		/*
		 * This is dynamic but it is not clean. Using the factory pattern, we will achieve
		 * both dynamism and cleanliness.
		 * 
		 * if(enemyShipOption.equals("U")){
		 * 
		 * 		theEnemy = new UFOEnemyShip();
		 * 
		 * } else if(enemyShipOption.equals("R")){
		 * 	
		 * 		theEnemy = new RocketEnemyShip();
		 * 
		 * }
		 * 
		 * */
		
		if(theEnemy != null){
			
			doStuffEnemy(theEnemy);
			
		} else {
			
			System.out.println("Enter U or R or B next time!");
			
		}
	}	
	
	public static void doStuffEnemy(EnemyShip anEnemyShip){
		
		anEnemyShip.displayEnemyShip();
		anEnemyShip.followHeroShip();
		anEnemyShip.enemyShipShoots();
		
	}
	
}
