package factory;

public class UFOEnemyShip extends EnemyShip{
	
	@SuppressWarnings("unused")
	private String idCode= "100";
	
	@SuppressWarnings("unused")
	private String getPrivate(){
		
		return "How did you get this";
		
	}
	@SuppressWarnings("unused")
	private String getOtherPrivate(int thisInt, String thatString){
		
		return "How did you get here "+ thisInt + " "+ thatString;
		
	}
	public UFOEnemyShip(int number, String randString){
		
		System.out.println("You sent: "+number+" "+randString);
		
	}
	public UFOEnemyShip() {
		super();
		
		setName("UFO Enemy ship");
		setDamage(20.0);
	}
	
}
