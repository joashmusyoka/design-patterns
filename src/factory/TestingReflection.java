package factory;

import java.lang.reflect.Modifier;

public class TestingReflection {
	
	public static void main(String[] args){
		
		Class<UFOEnemyShip> reflectClass = UFOEnemyShip.class;
		
		String className = reflectClass.getName();
		
		System.out.println(className + "\n");
		
		int classModifier = reflectClass.getModifiers();
		
		System.out.println(Modifier.isPublic(classModifier) + "\n");
	}
	
}
