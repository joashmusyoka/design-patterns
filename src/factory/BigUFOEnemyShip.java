package factory;

public class BigUFOEnemyShip extends UFOEnemyShip{
	
	public BigUFOEnemyShip() {
		super();
		
		setName("Big UFO Enemy ship");
		setDamage(40.0);
	}
	
}
