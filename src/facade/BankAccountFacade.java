package facade;

public class BankAccountFacade {
	private int accountNumber;
	private int securityCode;

	AccountNumberCheck acctChecker;
	SecurityCodeCheck codeChecker;
	FundsCheck fundsChecker;
	WelcomeToBank bankWelcome;

	public BankAccountFacade(int accountNumber, int securityCode) {
		this.accountNumber = accountNumber;
		this.securityCode = securityCode;

		bankWelcome = new WelcomeToBank();
		fundsChecker = new FundsCheck();
		codeChecker = new SecurityCodeCheck();
		acctChecker = new AccountNumberCheck();
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public int getSecurityCode() {
		return securityCode;
	}

	public void withdrawCash(double cashToGet) {
		if (acctChecker.accountActive(getAccountNumber())
				&& codeChecker.isCodeCorrect(getSecurityCode())
				&& fundsChecker.haveEnoughMoney(cashToGet)) {
			System.out.println("Transaction Complete\n");
		} else {
			System.out.println("Transaction failed. Cannot withdraw\n");
		}
	}

	public void depositCash(double cashToDeposit) {
		if (acctChecker.accountActive(getAccountNumber())
				&& codeChecker.isCodeCorrect(getSecurityCode())) {
			fundsChecker.makeDeposit(cashToDeposit);
			System.out.println("Transaction Complete\n");
		} else {
			System.out.println("Transaction failed. Cannot deposit\n");
		}
	}
}
