package facade;

public class AccountNumberCheck {
	private int accountNumber = 12345678;

	public int getAccountNumber() {
		return accountNumber;
	}

	public boolean accountActive(int accountNumberToCheck) {
		if (accountNumberToCheck == getAccountNumber()) {
			System.out.println("Acc number ok.");
			return true;
		}
		return false;
	}
}
