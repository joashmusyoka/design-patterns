package facade;

public class SecurityCodeCheck {
	int securityCode = 1234;

	public int getSecurityCode() {
		return securityCode;
	}

	public boolean isCodeCorrect(int codeToCheck) {
		if (codeToCheck == getSecurityCode()) {
			System.out.println("Security code ok.");
			return true;
		}
		return false;
	}
}
