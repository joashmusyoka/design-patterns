package iterator;

import java.util.Arrays;
import java.util.Iterator;

public class SongsOfThe80s implements SongIterator{
	private SongInfo[] bestSongs;
	
	int arrayKey = 0;
	public SongsOfThe80s() {
		bestSongs = new SongInfo[3];

		addSong("Roam", "B52s", 1989);
		addSong("American Pie", "Don McLean", 1984);
		addSong("I Will Serve", "Gloria Gaynor", 1985);
	}

	public void addSong(String songName, String bandName, int yearReleased) {
		SongInfo songInfo = new SongInfo(songName, bandName, yearReleased);
		
		bestSongs[arrayKey] = songInfo;
		
		arrayKey++;
	}
	
	/*
	public SongInfo[] getBestSongs() {
		return bestSongs;
	}*/

	@Override
	public Iterator<SongInfo> createIterator() {
		return Arrays.asList(bestSongs).iterator();
	}
}
