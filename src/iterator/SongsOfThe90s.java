package iterator;

import java.util.Hashtable;
import java.util.Iterator;

public class SongsOfThe90s implements SongIterator{
	private Hashtable<Integer, SongInfo> bestSongs = new Hashtable<Integer, SongInfo>();

	int hashKey = 0;
	
	public SongsOfThe90s() {

		addSong("Loosing My Religion", "REM", 1991);
		addSong("American Pie", "Don McLean", 1993);
		addSong("I Will Serve", "Gloria Gaynor", 1991);
	}

	public void addSong(String songName, String bandName, int yearReleased) {
		SongInfo songInfo = new SongInfo(songName, bandName, yearReleased);
		
		bestSongs.put(hashKey, songInfo);
		hashKey++;
	}
	/*
	public Hashtable<Integer, SongInfo> getBestSongs() {
		return bestSongs;
	}*/

	@Override
	public Iterator<SongInfo> createIterator() {
		return bestSongs.values().iterator();
	}
}
