package iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class SongsOfThe70s implements SongIterator{
	ArrayList<SongInfo> bestSongs;

	public SongsOfThe70s() {
		bestSongs = new ArrayList<SongInfo>();

		addSong("Imagine", "John Lennon", 1971);
		addSong("American Pie", "Don McLean", 1971);
		addSong("I Will Serve", "Gloria Gaynor", 1979);
	}

	public void addSong(String songName, String bandName, int yearReleased) {
		SongInfo sonfInfo = new SongInfo(songName, bandName, yearReleased);
		bestSongs.add(sonfInfo);
	}

	/*Old code
	 * public ArrayList<SongInfo> getBestSongs() {
		return bestSongs;
	}*/

	@Override
	public Iterator<SongInfo> createIterator() {
		return bestSongs.iterator();
	}
}
