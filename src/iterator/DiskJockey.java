package iterator;

import java.util.Iterator;

public class DiskJockey {
	/*
	 * SongsOfThe70s songs70s;
	 * SongsOfThe80s songs80s;
	 * SongsOfThe90s songs90s;
	*/
	
	SongIterator iter70sSongs;
	SongIterator iter80sSongs;
	SongIterator iter90sSongs;

	public DiskJockey(SongIterator newSongs70s, SongIterator newSongs80s,
			SongIterator newSongs90s) {
		iter70sSongs = newSongs70s;
		iter80sSongs = newSongs80s;
		iter90sSongs = newSongs90s;
	}
	
	/*
	 * Without the Iterator design patter you will Have to write all this!!!!
	 * 
	 * public void showTheSongs(){
		ArrayList<SongInfo> al70sSongs = songs70s.getBestSongs();
		
		System.out.println("Songs of the 70s:\n");
		for(SongInfo songInfo: al70sSongs){
			System.out.println(songInfo.getSongName());
			System.out.println(songInfo.getBandName());
			System.out.println(songInfo.getYearReleased());
		}
		
		SongInfo[] array80sSongs = songs80s.getBestSongs();
		
		System.out.println("Songs of the 80s:\n");
		for(SongInfo songInfo: array80sSongs){
			System.out.println(songInfo.getSongName());
			System.out.println(songInfo.getBandName());
			System.out.println(songInfo.getYearReleased());
		}
		
		Hashtable<Integer, SongInfo> ht90sSongs = songs90s.getBestSongs();
		
		System.out.println("Songs of the 90s:\n");
		for(Enumeration<Integer> e = ht90sSongs.keys(); e.hasMoreElements();){
			SongInfo songInfo = ht90sSongs.get(e.nextElement());
			
			System.out.println(songInfo.getSongName());
			System.out.println(songInfo.getBandName());
			System.out.println(songInfo.getYearReleased());
		}
	}*/
	
	public void showTheSongs2(){
		Iterator<SongInfo> songs70s = iter70sSongs.createIterator();
		Iterator<SongInfo> songs80s = iter80sSongs.createIterator();
		Iterator<SongInfo> songs90s = iter90sSongs.createIterator();
		
		System.out.println("Songs of the 70's\n");
		printTheSongs(songs70s);
		
		System.out.println("Songs of the 80's\n");
		printTheSongs(songs80s);
		
		System.out.println("Songs of the 79's\n");
		printTheSongs(songs90s);
	}
	public void printTheSongs(Iterator<SongInfo> iterator){
		while(iterator.hasNext()){
			SongInfo songInfo = iterator.next();
			
			System.out.println(songInfo.getSongName());
			System.out.println(songInfo.getBandName());
			System.out.println(songInfo.getYearReleased());
		}
	}
}