package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorController {
	private CalculatorView theView;
	private CalculatorModel theModel;

	public CalculatorController(CalculatorView theView, CalculatorModel theModel) {
		this.theModel = theModel;
		this.theView = theView;

		this.theView.addCalculationListener(new CalculateListener());
	}

	class CalculateListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int firstNumber, secondNumber = 0;

			try {
				firstNumber = theView.getFirstNumber();
				secondNumber = theView.getSecondNumber();

				theModel.addTwoNumbers(firstNumber, secondNumber);
				theView.setCalculationSolution(theModel.getCalculationValue());
			} catch (NumberFormatException e2) {
				theView.displayError("You need to enter 2 integers!");
			}

		}

	}
}
