package mvc;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalculatorView extends JFrame {

	private static final long serialVersionUID = 7748142206300372809L;

	private JTextField firstNumber = new JTextField(10);
	private JLabel additionLabel = new JLabel("+");
	private JTextField secondNumber = new JTextField(10);

	private JButton calculateButton = new JButton("Calculate");
	private JTextField calculationSolution = new JTextField(10);

	CalculatorView() {
		JPanel calcPanel = new JPanel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 200);

		calcPanel.add(firstNumber);
		calcPanel.add(additionLabel);
		calcPanel.add(secondNumber);

		calcPanel.add(calculateButton);
		calcPanel.add(calculationSolution);

		this.add(calcPanel);

	}

	public int getFirstNumber() {
		return Integer.parseInt(firstNumber.getText());
	}

	public int getSecondNumber() {
		return Integer.parseInt(secondNumber.getText());
	}

	public int getCalculationSolution() {
		return Integer.parseInt(calculationSolution.getText());
	}

	public void setCalculationSolution(int solution) {
		calculationSolution.setText(Integer.toString(solution));
	}

	void addCalculationListener(ActionListener calculationButtonListener) {
		calculateButton.addActionListener(calculationButtonListener);
	}

	void displayError(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
}