package oop;

public class Monkey implements Living {

	@Override
	public void setName(String name) {

	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public void setWeight(int weight) {

	}

	@Override
	public String getWeight() {
		return null;
	}

	@Override
	public void setHeight(double height) {

	}

	@Override
	public double getHeight() {
		return 0;
	}

	@Override
	public void setFavFood(String food) {

	}

	@Override
	public String getFavFood() {
		return null;
	}

	@Override
	public void setSpeed(double speed) {

	}

	@Override
	public double getSpeed() {
		return 0;
	}

	@Override
	public void setSound(String sound) {

	}

	@Override
	public String getSound() {
		return null;
	}

}
