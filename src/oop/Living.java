package oop;

public interface Living {
	public void setName(String name);
	public String getName();
	
	public void setWeight(int weight);
	public String getWeight();
	
	public void setHeight(double height);
	public double getHeight();
	
	public void setFavFood(String food);
	public String getFavFood();
	
	public void setSpeed(double speed);
	public double getSpeed();
	
	public void setSound(String sound);
	public String getSound();
}
