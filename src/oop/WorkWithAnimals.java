package oop;

public class WorkWithAnimals {

	public static void main(String[] args) {
		Animal doggy = new Dog();
		Animal kitty = new Cat();
		
		System.out.println("Doggy says: "+doggy.getSound());
		System.out.println("kitty says: "+kitty.getSound());
		
		Animal[] animals = new Animal[4];
		animals[0] = doggy;
		animals[1] = kitty;
		
		System.out.println("Doggy says: "+animals[0].getSound());
		System.out.println("kitty says: "+animals[1].getSound());
		
		speakAnimal(doggy);
		
		//doggy.digHole(); Does not work
		((Dog)doggy).digHole();
		
		Giraffe giraffe = new Giraffe();
		giraffe.setName("Peter");
		System.out.println(giraffe.getName());
	}

	public static void speakAnimal(Animal a){
		System.out.println("Animal says: "+a.getSound());
	}
}
