========================================What is a class?========================================
-> A class is a blueprint.
-> Fields - Instance variables
	- What an object knows
-> Methods - Functions 
    - What an object does
 
e.g                     [        Animal       ]
                        [name: String         ]
                        [height: double       ]
                        [weight: int          ]
                        [favouriteFood: String]
                        [speed: double        ]
                        ______________________
                        [Animal(): Void       ]
                        [move(): int          ]
                        [eat(): void          ]
                        [setName(String): void]
                        [getName(): String    ]
When to use inheritance?
-> The subclass is a super class - A dog is an animal
-> When a subclass needs most of the methods in a super class
   - Almost all the methods in animal is used in dog.
-> Don't use inheritance just to reuse code, or they don't have a IS A relationship
-> Avoid duplicate code.
-> Changes to the super class code is instantly reflected to the sub-classes
-> User knows that all the sub classes have all the methods of the super class.

==========================================What is Polymorphism?================================
-> Polymorphism allows you to write methods that do not need to change if new sub-classes are
   created.
-> E.g. Dog can add a new class without changing animal.
-> Also if Dog wants to override a method it can do so without affecting animal in any way.
-> You can put different classes in an array.
-> E.g. Animal doggy = new Dog(), Animal kitty = new Cat() can be put in the same array.
-> kitty.getSound() executes the Cat method.
-> You cannot access methods this way if they are only in the subclass though.
==============================What is an abstract class?=======================================
-> If you want the power of polymorphism without the work.
-> abstract public class Creature.
-> public abstract void setName();
-> There are no abstract fields.
-> All methods do not have to be abstract
-> You can have static methods
==============================What is an interface?============================================
-> An interface is a class with only abstract methods.
-> You can add as many interfaces as you want in a class using implements
-> You can only use public static and final fields.
-> Interfaces provide the ultimate in flexibility.
   - Classes from different inheritance trees can use a common interface.