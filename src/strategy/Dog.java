package strategy;

public class Dog extends Animal{
	public void digHole(){
		
		System.out.println("Digging a Hole..");
		
	}
	public Dog(){
		
		super();
		
		setSound("Bark");
		
		flyingType = new CantFly();
	}
}
