package strategy;

public class Animal {
	
	private String name;
	private double height;
	private int weight;
	private String favFood;
	private double speed;
	private String sound;
	
	public Flys flyingType;
	
	public String getName() {
		return name;
	}
	public void setName(String newName) {
		this.name = newName;
	}
	
	public double getHeight() {
		return height;
	}
	public void setHeight(double newHeight) {
		if(newHeight > 0){
			
			this.height = newHeight;
			
		} else {
			
			System.out.println("Height must be greater than 0.");
			
		}
	}
	
	public int getWeight() {
		return weight;
	}
	public void setWeight(int newWeight) {
		if(newWeight > 0){
			
			this.weight = newWeight;
			
		} else {
			
			System.out.println("Weight must be greater than 0.");
			
		}
	}
	public String getFavFood() {
		return favFood;
	}
	public void setFavFood(String newFavFood) {
		this.favFood = newFavFood;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double newSpeed) {
		this.speed = newSpeed;
	}
	public String getSound() {
		return sound;
	}
	public void setSound(String newSound) {
		this.sound = newSound;
	}
	
	public String tryToFly(){
		return flyingType.fly();
	}
	
	public void setFlyingAbility(Flys newFlyingType){
		
		this.flyingType = newFlyingType;
		
	}
}
