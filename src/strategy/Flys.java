package strategy;

public interface Flys {
	String fly();
}
class ItFlys implements Flys{

	@Override
	public String fly() {
		
		return "I am flying high.";
		
	}
}
class CantFly implements Flys{

	@Override
	public String fly() {
		
		return "I can't fly.";
		
	}
}