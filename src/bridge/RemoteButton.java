package bridge;

public abstract class RemoteButton {
	// composition
	private EntertainmentDevice theDevice;

	public RemoteButton(EntertainmentDevice newDevice) {
		theDevice = newDevice;
	}

	public void buttonFivePressed() {
		theDevice.buttonFivePress();
	}
	
	public void buttonSixPressed() {
		theDevice.buttonSixPress();
	}
	public void deviceFeedback(){
		theDevice.deviceFeedback();
	}
	public abstract void buttonNinePressed();
}
