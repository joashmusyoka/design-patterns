package bridge;

public class TVDevice extends EntertainmentDevice {
	public TVDevice(int newDeviceState, int newMaxSetting) {
		this.deviceState = newDeviceState;
		this.maxSetting = newMaxSetting;
	}

	@Override
	public void buttonFivePress() {
		System.out.println("Channel Down...");
		
		deviceState--;
	}

	@Override
	public void buttonSixPress() {
		System.out.println("Channel Up...");
	}

}
