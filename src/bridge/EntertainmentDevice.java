package bridge;

/*
 * This represents the abstract device
 * In this case it can be: A TV, DVD Player or a Remote Control
 * */
public abstract class EntertainmentDevice {
	public int deviceState;// current channel or current chapter
	public int maxSetting;// max channel or max chapter
	public int volumeLevel = 0;

	public abstract void buttonFivePress();

	public abstract void buttonSixPress();

	public void deviceFeedback() {
		if (deviceState > maxSetting || deviceState < 0) {
			deviceState = 0;

			System.out.println("On " + deviceState);
		}
	}

	public void buttonSevenPressed() {
		volumeLevel++;
		
		System.out.println("Volumen at "+volumeLevel);
	}
	
	public void buttonEightPressed() {
		volumeLevel--;
		
		System.out.println("Volume at "+volumeLevel);
	}
}
