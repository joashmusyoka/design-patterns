package memento;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class TestMemento extends JFrame {

	private static final long serialVersionUID = 3356343147226638580L;
	private JButton save, undo, redo;
	private JTextArea theArticle = new JTextArea(40, 60);
	Caretaker caretaker = new Caretaker();
	Originator originator = new Originator();
	int savedFiles = 0, currentArticle = 0;

	public static void main(String[] args) {
		new TestMemento();
	}

	public TestMemento() {
		this.setSize(750, 780);
		this.setTitle("Articles");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel1 = new JPanel();
		panel1.add(new JLabel("Article"));
		panel1.add(theArticle);

		ButtonListener saveListener = new ButtonListener();
		ButtonListener undoListener = new ButtonListener();
		ButtonListener redoListener = new ButtonListener();

		save = new JButton("Save");
		save.addActionListener(saveListener);
		undo = new JButton("Undo");
		undo.addActionListener(undoListener);
		redo = new JButton("Redo");
		redo.addActionListener(redoListener);

		panel1.add(save);
		panel1.add(undo);
		panel1.add(redo);

		this.add(panel1);
		this.setVisible(true);
	}

	class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == save) {
				// save
				String textInTextArea = theArticle.getText();
				originator.set(textInTextArea);
				caretaker.addMemento(originator.storeInMemento());
				savedFiles++;
				currentArticle++;

				System.out.println("Saved Files: " + savedFiles);
				undo.setEnabled(true);
			}
			if (e.getSource() == undo) {
				if (currentArticle >= 1) {
					currentArticle--;
					String textAreaString = originator
							.restoreFromMemento(caretaker
									.getMemento(currentArticle));
					theArticle.setText(textAreaString);
					redo.setEnabled(true);
				} else {
					undo.setEnabled(false);
				}
			}
			if (e.getSource() == redo) {
				if ((savedFiles - 1) > currentArticle) {
					currentArticle++;
					String textAreaString = originator
							.restoreFromMemento(caretaker
									.getMemento(currentArticle));
					theArticle.setText(textAreaString);
					undo.setEnabled(true);
				} else {
					redo.setEnabled(false);
				}
			}
		}

	}
}
