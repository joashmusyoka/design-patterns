package mediator;

public class GormanSlacks extends Colleague {
	public GormanSlacks(Mediator mediator) {
		super(mediator);
		
		System.out.println("Gorman Slack signed up for the exchange.\n");
	}
}
