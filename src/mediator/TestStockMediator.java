package mediator;

public class TestStockMediator {

	public static void main(String[] args) {
		StockMediator nse = new StockMediator();
		
		GormanSlacks broker1 = new GormanSlacks(nse);
		JTPoorman broker2 = new JTPoorman(nse);
		nse.addColleague(broker1);
		nse.addColleague(broker2);
		
		broker1.saleOffer("SFCOM", 100);
		broker1.saleOffer("EQTY", 200);
		broker1.saleOffer("EABL", 50);
		broker1.saleOffer("MSFT", 250);
		
		broker2.buyOffer("MSFT", 250);
		broker2.saleOffer("NRG", 10);
		
		broker1.buyOffer("NRG", 10);
		
		nse.getStockOfferings();
	}

}
